# Team HU-externals: the Git repository for the class "Data Science in Life Sciences", summer semester 2020, Freie Universität zu Berlin

Welcome to our official repository!

## About our team

We are two biophysics at the Humboldt University of Berlin who are interested in bioinformatics and machine learning.
Due the fact that we are taking this class part-time, Prof. Conrad tasked us with completing four weekly tasks (out of 12) to get 5 credit points.

## General Information about this repository

During this course our team covered the four the following topics:
- **Week 4: MLSP-GUI - a tool for SARS-CoV-2 genomic origin investigation**
- **Week 7: Time series analysis and prediction of Berlin COVID-19 cases**
- **Week 11: Sequence-curve based Phylogeny Analysis for SARS-CoV-2**
- **Week 12: Drug repurposing and COVID-19 drug exploration**

For each weekly task a corresponding folder can be found in this repository. Each folder contains the data and that we used for the analysis task along with notebooks containing any self-written code which was used. 
In addition, a report which offers a detailed overview about the work we did over these four weeks and the results of our analysis can also be found under the folder 'Main_Report'.


