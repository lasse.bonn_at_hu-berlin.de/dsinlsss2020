# HU-EXTERNALS: week 7 - time series forecasting with three methods.

## the actual code 

We are using google colab [here](https://colab.research.google.com/drive/1M-EzpHSWudKH4RkKO3vtvqCvj_yZwH49?usp=sharing), so you can use this link to run the notebook directly on the cloud.
We have uploaded the .ipynb but it is probably better to run in colab because of dependencies etc.
# How to run:

Just restart the kernel and re-run the code to recreate the work that is mentioned in our report.

# Info about the contents of the notebook:

This notebook is devided to 4 parts:
  - Fetching the data and visualizing it
  - The analysis via seasonal ARIMA
  - The analysis via Prophet
  - The analusis via XBoost ("the ML approach")